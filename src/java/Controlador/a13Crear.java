/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import DAO.MunicipioDao;
import DAO.PersonaDao;
import Modelo.Municipios;
import Modelo.Persona;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Harold
 */
public class a13Crear extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int ced = Integer.parseInt(request.getParameter("ced"));
        int id = Integer.parseInt(request.getParameter("id"));
        PersonaDao per = new PersonaDao();
        MunicipioDao mun = new MunicipioDao();
        Persona p = per.read(ced);
        if (p == null) {
            String name = request.getParameter("name");
            name = name.toUpperCase().charAt(0) + name.substring(1,name.length()).toLowerCase();
            String email = request.getParameter("email");
            email = email.toUpperCase().charAt(0) + email.substring(1,email.length()).toLowerCase();
            String dir = request.getParameter("dir");
            p = new Persona(ced,dir,email,name,mun.read(id));
            per.create(p);
            request.getRequestDispatcher("a13.jsp").forward(request, response);
        } else {
            request.getSession().setAttribute("ced", ced);
            Municipios m = mun.read(id);
            request.getSession().setAttribute("idDpto", m.getIdDpto().getIdDpto());
            request.getRequestDispatcher("Error-a1.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
