/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelo.Departamento;
import Persistencia.DepartamentoJpaController;
import java.util.List;

/**
 *
 * @author Harold
 */
public class DepartamentoDao {
    DepartamentoJpaController dep;
    
    public DepartamentoDao(){
        Conexion con = Conexion.getCon();
        this.dep = new DepartamentoJpaController(con.getBd());
    }
    
    public List<Departamento> readDpto(){
        return this.dep.findDepartamentoEntities();
    }
}
