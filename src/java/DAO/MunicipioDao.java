/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelo.Municipios;
import Persistencia.MunicipiosJpaController;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Harold
 */
public class MunicipioDao {

    MunicipiosJpaController mun;

    public MunicipioDao() {
        Conexion con = Conexion.getCon();
        this.mun = new MunicipiosJpaController(con.getBd());
    }

    public List<Municipios> readMun(int idDpto) {
        List<Municipios> nueva = new ArrayList();
        List<Municipios> muns = mun.findMunicipiosEntities();
        for (Municipios m : muns) {
            if (m.getIdDpto().getIdDpto() == idDpto) {
                nueva.add(m);
            }
        }
        return nueva;
    }

    public Municipios read(int id) {
        return mun.findMunicipios(id);
    }
}
