/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Modelo.Persona;
import Persistencia.MunicipiosJpaController;
import Persistencia.PersonaJpaController;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Harold
 */
public class PersonaDao {

    PersonaJpaController per;

    public PersonaDao() {
        Conexion con = Conexion.getCon();
        this.per = new PersonaJpaController(con.getBd());
    }
    
    public void create(Persona p){
        try {
            this.per.create(p);
        } catch (Exception ex) {
            Logger.getLogger(PersonaDao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Persona read(int id){
        return this.per.findPersona(id);
    }
    
    public List<Persona> readPersonas(int idMun){
        List<Persona> pers = this.per.findPersonaEntities();
        List<Persona> nuevo = new ArrayList();
        for(Persona p: pers){
            if(p.getIdMunicipio().getIdMunicipio().equals(idMun))
                nuevo.add(p);
        }
        return nuevo;
    }
    
    public List<Persona> read(){
        return this.per.findPersonaEntities();
    }
    
    public Persona readPersonName(String nombre){
        List<Persona> per = this.read();
        for(Persona p: per){
            if(p.getNombre().equals(nombre))
                return p;
        }
        return null;
    }

}
