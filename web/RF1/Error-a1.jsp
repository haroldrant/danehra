<%-- 
    Document   : a13
    Created on : 17/06/2021, 02:01:45 PM
    Author     : Harold
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    int ced = (Integer) request.getSession().getAttribute("ced");
    int dpto = (Integer) request.getSession().getAttribute("idDpto");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="5;URL=a12Crear.do?id=<%=dpto%>">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>ERROR AL CREAR</title>
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
        <main>
            <div class="container">
                <div class="justify-content-center text-center">
                    <div class="row col">                        
                        <h4>Error al crear cédula <%=ced%> ya existe</h4>
                    </div>
                    <div class="row col">
                        <h4>Será redireccionado en 5 segundos</h4>
                    </div>
                </div>                
            </div>
        </main>
        <footer>
            <div class="container">
                <div class="row text-center">
                    <h5>Harold Rueda - 1151904</h5>
                </div>                
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
                integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
                integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
    </body>
</html>
