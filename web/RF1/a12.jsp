<%-- 
    Document   : a12
    Created on : 17/06/2021, 01:55:29 PM
    Author     : Harold
--%>

<%@page import="java.util.List"%>
<%@page import="Modelo.Municipios"%>
<!DOCTYPE html>
<html lang="es" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>REGISTRAR PERSONA | DANE</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/styles.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
    </head>
    <body>
        <main>
            <div class="container">
                <div class="cuad">
                    <form action="a13Crear.do" method="post">
                        <div class="row  justify-content-center">
                            <div class="col-2">
                                <label class="form-label">C�dula: </label>
                            </div>
                            <div class="col-3">
                                <input type="number" class="form-control" name="ced" required min="0">
                            </div>
                        </div>
                        <br>
                        <div class="row  justify-content-center ">
                            <div class="col-2">
                                <label class="form-label">Nombre: </label>
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control" name="name" required>
                            </div>
                        </div>
                        <br>
                        <div class="row  justify-content-center ">
                            <div class="col-2">
                                <label class="form-label">E-mail: </label>
                            </div>
                            <div class="col-3">
                                <input type="email" class="form-control" name="email" required>
                            </div>
                        </div>
                        <br>
                        <div class="row  justify-content-center ">
                            <div class="col-2">
                                <label class="form-label">Direcci�n: </label>
                            </div>
                            <div class="col-3">
                                <input type="text" class="form-control" name="dir" required>
                            </div>
                        </div>
                        <br>
                        <div class="row justify-content-center ">
                            <div class="col-2">
                                <label class="form-label">Municipio: </label>
                            </div>
                            <div class="col-3">
                                <select class="form-select" name="id">
                                    <%
                                        List<Municipios> m = (List) request.getSession().getAttribute("muns");
                                        for (Municipios mun : m) {
                                    %>
                                    <option value="<%=mun.getIdMunicipio()%>"><%=mun.getNombre()%></option>
                                    <%
                                        }
                                    %>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="row justify-content-center">
                            <div class="col-2">
                                <button type="submit" class="btn btn-info">Registrar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        <footer>
            <div class="container">
                <div class="row text-center">
                    <h5>Harold Rueda - 1151904</h5>
                </div>
                <div class="row">
                    <div class="offset-11">
                        <a href="../index.html" class="home-icon"><i class="fas fa-home"></i></a>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
                integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
                integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
    </body>
</html>
