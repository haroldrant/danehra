<%-- Document : a1 Created on : 17/06/2021, 01:49:43 PM Author : Harold --%>

<%@page import="Modelo.Municipios"%>
<%@page import="java.util.List" %>
<%@page import="Modelo.Departamento" %>
<%@page import="Modelo.Departamento" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>MOSTRAR INFORMACIÓN PERSONA | DANE</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
              rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
              crossorigin="anonymous">
        <link rel="stylesheet" href="../css/styles.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
    </head>

    <body>
        <div class="container">
            <header>
                <h1>DANE</h1>
            </header>
            <main>
                <div class="cuad">
                    <div class="row justify-content-center text-center">
                        <table class="table table-hover table-light">
                            <thead>
                                <tr>
                                    <th scope="col">MUNICIPIO</th>
                                    <th scope="col">CANTIDAD DE PERSONAS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    List<Municipios> muns = (List) request.getSession().getAttribute("muns");
                                    List<Integer> cantidad = (List) request.getSession().getAttribute("cant");
                                    for (int i = 0; i < muns.size(); i++) {
                                %>
                                <tr>
                                    <th scope="row"><%=muns.get(i).getNombre() %></th>
                                    <td><%=cantidad.get(i).intValue() %></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
        </div>
        <footer>
            <div class="container">
                <div class="row text-center">
                    <h5>Harold Rueda - 1151904</h5>
                </div>
                <div class="row">
                    <div class="offset-11">
                        <a href="../index.html" class="home-icon"><i class="fas fa-home"></i></a>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
                integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
                integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
    </body>

</html>