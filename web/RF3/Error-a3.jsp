<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="refresh" content="5;URL=a3.jsp">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
              integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <title>ERROR AL BUSCAR</title>
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body>
        <main>
            <div class="container">
                <div class="justify-content-center text-center">
                    <div class="row col">
                        <%
                            String name = (String)request.getSession().getAttribute("nombre");
                        %>
                        <h4>Error persona llamada <%=name %> no existe</h4>
                    </div>
                    <div class="row col">
                        <h4>Sera redireccionado en 5 segundos</h4>
                    </div>
                </div>                
            </div>
        </main>
        <footer>
            <div class="container">
                <div class="row text-center">
                    <h5>Harold Rueda - 1151904</h5>
                </div>                
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
                integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
        crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js"
                integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT"
        crossorigin="anonymous"></script>
    </body>
</html>
